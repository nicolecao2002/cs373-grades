#!/usr/bin/env python3

# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = line-too-long

# ---------
# Grades.py
# ---------

# https://docs.python.org/3.6/library/functools.html
# https://sphinx-rtd-tutorial.readthedocs.io/en/latest/docstrings.html

# -----------
# grades_eval
# -----------

# create mapping between grade precent and letter grade
grade_precent_map = {
    94: "A",
    90: "A-",
    87: "B+",
    84: "B",
    80: "B-",
    77: "C+",
    74: "C",
    70: "C-",
    67: "D+",
    64: "D",
    60: "D-",
}

# create mapping between number of 2 & 3 and precent for project
percent_project_map = {5: 94, 4: 87, 3: 67}

# create mapping between number of 2 & 3 and precent for exercises.
percent_exercise_map = {12: 94, 11: 94, 10: 87, 9: 77, 8: 70, 7: 60}

# create mapping between number of 2 & 3 and precent for blog and paper.
percent_blog_map = {14: 94, 13: 94, 12: 87, 11: 80, 10: 74, 9: 67, 8: 60}

percent_paper_map = {14: 94, 13: 94, 12: 87, 11: 80, 10: 74, 9: 67, 8: 60}

# create mapping between number of 2 & 3 and precent for quizzes.
percent_quizzes_map = {
    42: 94,
    41: 94,
    40: 94,
    39: 94,
    38: 90,
    37: 87,
    36: 84,
    35: 84,
    34: 80,
    33: 77,
    32: 77,
    31: 74,
    30: 70,
    29: 70,
    28: 67,
    27: 64,
    26: 60,
    25: 60,
}

# a collection of mapping we need for calculate the grade
mapList = [
    percent_project_map,
    percent_exercise_map,
    percent_blog_map,
    percent_paper_map,
    percent_quizzes_map,
]


# calculate the precent based on the current category
def calculate_grades(current_list: list[int], grade_map) -> int:
    assert current_list
    assert grade_map
    count_project_1 = 0
    count_project_2 = 0
    count_project_3 = 0
    # count points of each category
    for points in current_list:
        if points == 1:
            count_project_1 += 1
        elif points == 2:
            count_project_2 += 1
        elif points == 3:
            count_project_3 += 1
    # add the total points of 2 and 3's plus the number of 1 we can revise from
    # pair of 3's
    total_point = (
        count_project_2 + count_project_3 + min(count_project_3 // 2, count_project_1)
    )

    # now that we finish counting
    grade = grade_map.get(total_point)
    if grade is None:
        return 0

    # we got the mapping from the map
    assert grade >= 60
    return grade


# define the mapping
def grades_eval(l_l_scores: list[list[int]]) -> str:
    assert l_l_scores

    # If we miss any category, i.e. the , then we just return 'F'
    if len(l_l_scores) < 5:
        return "F"
    # create a list of precentage grade
    category_grade = []
    # iterate through the Map list and get the precentage from the number of 2's and 3's
    for i in range(0, 5):
        current_grade = l_l_scores[i]
        category_grade.append(calculate_grades(current_grade, mapList[i]))
    # iterate through the list, and find the lowest grade.
    min_val = category_grade[0]
    for precent_grade in category_grade:
        if precent_grade < min_val:
            min_val = precent_grade
        if min_val == 0:
            return "F"
    # post condition, we know we get the grade from the mapping
    assert min_val >= 60
    return str(grade_precent_map.get(min_val))
