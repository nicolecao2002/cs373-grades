# CS373: Software Engineering Grades Repo

* Name: Yingting Cao

* EID: yc29789

* GitLab ID: nicolecao2002

* HackerRank ID: nicole_cao1

* Git SHA: cfc36d94bf211a29754f5281738c76de5e6f4b0b

* GitLab Pipelines: https://gitlab.com/nicolecao2002/cs373-grades/-/pipelines/1159855181

* Estimated completion time: 15 hours

* Actual completion time: 21 hours

* Comments: The makefile is very helpful and organized.
